""" Auxiliary structure composed of a set of inverted indexes that allows 
addition of a set od documents and posterior querying, capable of handling
multiple boolean operations, phrase queries and multi-index queries.
"""


__all__ = ['add_document', 'save', 'load', 'search', 'tf_idf']
__author__ = 'Jose Carneiro'


from collections import defaultdict, deque, Counter
import math
import re

import pickle
import numpy as np
import config as cfg

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, RegexpTokenizer
from nltk.stem import PorterStemmer, SnowballStemmer

from config import SearchType


class SearchStructure:
    """ Main structure used to index and allow queries """

    def __init__(self, tokenizer=word_tokenize, stemmer=SnowballStemmer('english'), stop_words=stopwords.words('English')):

        # inverse index for all supported categories
        self.title_inv_index = defaultdict(dict)
        self.publication_inv_index = defaultdict(dict)
        self.author_inv_index = defaultdict(dict)
        self.content_inv_index = defaultdict(dict)

        # pattern matchers for category searches
        self.boolean_pattern = re.compile(r"(?:(?<=^)|(?<=AND|NOT)|(?<=OR))(.*?)(?=NOT|AND|OR|$)")
        self.operator_pattern = re.compile(r"(AND|OR|NOT)")
        self.author_pattern = re.compile(r"(?<=author:)(.*?\s?)(?:content:|author:|publication:|title:|$)")
        self.publication_pattern = re.compile(r"(?<=publication:)(.*?\s?)(?:content:|author:|publication:|title:|$)")
        self.title_pattern = re.compile(r"(?<=title:)(.*?\s?)(?:content:|author:|publication:|title:|$)")
        self.content_pattern = re.compile(r"(?<=content:)(.*?\s?)(?:content:|author:|publication:|title:|$)")
        
        # initialization options
        self.stop_words = stop_words
        self.tokenizer = tokenizer
        self.stemmer = stemmer

        # auxiliary counter for keeping track of the number of documents and document n_words
        self.n_documents = 0

    def add_document(self, id, author, title, publication, content):
        """ Add Document to the datastructure. Adds relevant information to
        the corresponding inverted index structure for each category. 
        
        Parameters:
        -----------
            id : String
                unique identifier of the document
            author : String
                author of the document
            title : String
                title of the document
            publication : String
                publication of the document
            content : String
                contents of the document
        """
        # sanitize the string for each category
        words_title = self.sanitize(title, stem=cfg.TITLE_STEM,
                                    stopword_filter=cfg.TITLE_STOP,
                                    casefolding=cfg.TITLE_CASEFOLD)
        words_publication = self.sanitize(publication, stem=cfg.PUBLICATION_STEM,
                                    stopword_filter=cfg.PUBLICATION_STOP,
                                    casefolding=cfg.PUBLICATION_CASEFOLD)
        words_author = self.sanitize(author, stem=cfg.AUTHOR_STEM,
                                    stopword_filter=cfg.AUTHOR_STOP,
                                    casefolding=cfg.AUTHOR_CASEFOLD)
        words_content = self.sanitize(content, stem=cfg.CONTENT_STEM,
                                    stopword_filter=cfg.CONTENT_STOP,
                                    casefolding=cfg.CONTENT_CASEFOLD)

        # add each word from each category to its corresponding inverted index
        # maintaining information on the word position for phrase queries 

        # Title
        for pos, word in enumerate(words_title):
            if id in self.title_inv_index[word]:
                self.title_inv_index[word][id].append(pos)
            else:
                self.title_inv_index[word][id] = [pos]
        
        # Publication
        for pos, word in enumerate(words_publication):
            if id in self.publication_inv_index[word]:
                self.publication_inv_index[word][id].append(pos)
            else:
                self.publication_inv_index[word][id] = [pos]
        
        # Author
        for pos, word in enumerate(words_author):
            if id in self.author_inv_index[word]:
                self.author_inv_index[word][id].append(pos)
            else:
                self.author_inv_index[word][id] = [pos]

        # Contents
        for pos, word in enumerate(words_content):
            if id in self.content_inv_index[word]:
                self.content_inv_index[word][id].append(pos)
            else:
                self.content_inv_index[word][id] = [pos]

        # increment the document counter
        self.n_documents += 1

    def sanitize(self, text, stem=False, stopword_filter=False, casefolding=False, is_query=False):
        """ Given a text input, returns the sanitized version of that text 
        
        Parameters:
        -----------
            text : string
                text to be sanitized
            stem : bool
                whether or not to stem the text
            stopword_filter : bool
                whether or not to remove stopwords
            casefolding : bool
                whether or not to convert all words to lowercase
            is_query : bool
                whether or not the text is a query
        Return:
        -------
            tokens : array-like(string)
                array containing all the sanitized words obtained from the text
        """
        # tokenize the text in accord with it being a query or not, also removes punctuation
        if is_query:
            tokens = RegexpTokenizer(cfg.QUERY_TOKENIZER).tokenize(text)
        else:
            tokens = RegexpTokenizer(cfg.WORD_TOKENIZER).tokenize(text)

        # stemm the tokens if the option is active
        if stem:
            tokens = [self.stemmer.stem(term) for term in tokens]  
        
        # remove stopwords if stopwords filter is active
        if stopword_filter:
            tokens = [word for word in tokens if word not in self.stop_words]

        # conver to lowercase if the option is active (stemming already converts to lowercase)
        if casefolding and not stem:
            tokens = [term.lower() for term in tokens]

        # apply the same configurations to the phrase queries
        if is_query:
            tokens = [term if not re.compile(r'(".*?")').match(term) else ' '.join(self.sanitize(term, stem, stopword_filter, casefolding)) for term in tokens]

        return tokens

    def search(self, query_string, top_n=cfg.N_RESULTS, pretty_print=False):
        """ Searches the structure for the provided query. Starts by parsing
        boolean operators followed by category tags and then single terms
        or phrase queries. Returns only a subset of the results with the 
        highest ranked hits.

        Parameters:
        -----------
            query_string : str
            
            top_n : int
                how many documents to return

        Returns:
        --------
            total_hits : int
                returns the total number of hits for the given input query

            ranked_results : dict
                returns a dictionary containing the top document ids together
                with their corresponding score.
        """
        # check for boolean queries
        bool_queries = deque(self.boolean_pattern.findall(query_string))
        bool_operations = deque(self.operator_pattern.findall(query_string))
        result_sets = []

        # if no boolean queries, skip to "normal" search
        if bool_operations:
            
            # run searches for each "subquery" to fetch results
            for match in bool_queries:
                result_sets.append(self.category_search(match))

            # apply the operations in the queries
            while bool_operations:

                # get the next operation (the precedence on these is assumed 
                # to be the same, so go from left to right)
                next_op = bool_operations.popleft()
                if next_op == 'AND':
                    and_set = dict(result_sets[0]).keys() & dict(result_sets[1]).keys()
                    res = {doc_id:dict(Counter(dict(result_sets[0])) 
                           + Counter(dict(result_sets[1])))[doc_id] for doc_id in and_set}

                elif next_op == 'OR':
                    # OR operations 
                    or_set = dict(result_sets[0]).keys() | dict(result_sets[1]).keys()
                    res = {doc_id:dict(Counter(dict(result_sets[0])) 
                           + Counter(dict(result_sets[1])))[doc_id] for doc_id in or_set}

                elif next_op == 'NOT':
                    not_set = dict(result_sets[0]).keys() - dict(result_sets[1]).keys()
                    res = {doc_id:dict(Counter(dict(result_sets[0])))[doc_id] for doc_id in not_set}

                # update the structures
                del result_sets[0:2]
                result_sets.insert(0, res)
            
        else: 
            result_sets.append(self.category_search(query_string))

        if pretty_print:
            print("=" * 25)
            print(f"#{len(result_sets[0])} Results Found")
            print("-" * 25)
            [print(i[0], i[1]) for i in sorted(result_sets[0].items(), key=lambda kv: kv[1], reverse=True)[:top_n]]

            print("=" * 25)

        # return result_sets
        return len(result_sets[0]), sorted(result_sets[0].items(), key=lambda kv: kv[1], reverse=True)[:top_n]

    def category_search(self, query_string):
        """
        Parses and searches the corpus, handling any category tags and 
        performing the search on the adequate index.

        Parameters:
        -----------
            query_string : str
                the string to parse into a query

        Returns:
        --------
            ranked_results : dict
                a dictionary containing the id of the matching document 
                together with its corresponding score.
        """
        query_res = []

        # Look for matches for any category type queries and for each, get the
        #  hits author tag matches
        for match in self.author_pattern.findall(query_string):
            query_sanitized = self.sanitize(match, cfg.AUTHOR_STEM, 
                                            cfg.AUTHOR_STOP, cfg.AUTHOR_CASEFOLD, is_query=True)
            [query_res.append(self.unit_search(query, self.author_inv_index).keys()) for query in query_sanitized]
        # title tag matches
        for match in self.title_pattern.findall(query_string):
            query_sanitized = self.sanitize(match, cfg.TITLE_STEM, 
                                            cfg.TITLE_STOP, cfg.TITLE_CASEFOLD, is_query=True)
            [query_res.append(self.unit_search(query, self.title_inv_index).keys()) for query in query_sanitized]
        # publication tag matches
        for match in self.publication_pattern.findall(query_string):
            query_sanitized = self.sanitize(match, cfg.PUBLICATION_STEM, 
                                            cfg.PUBLICATION_STOP, cfg.PUBLICATION_CASEFOLD, is_query=True)
            [query_res.append(self.unit_search(query, self.publication_inv_index).keys()) for query in query_sanitized]
        # content tag matches
        for match in self.content_pattern.findall(query_string):
            query_sanitized = self.sanitize(match, cfg.CONTENT_STEM, 
                                            cfg.CONTENT_STOP, cfg.CONTENT_CASEFOLD, is_query=True)
            [query_res.append(self.unit_search(query, self.content_inv_index).keys()) for query in query_sanitized]

        # if no matches were found thus far, assume content search
        if not query_res:
            query_sanitized = self.sanitize(query_string, cfg.CONTENT_STEM, 
                                            cfg.CONTENT_STOP, cfg.CONTENT_CASEFOLD, is_query=True)
            [query_res.append(self.unit_search(query, self.content_inv_index).keys()) for query in query_sanitized]

            # check if it still has no results
            if not query_res:
                return {}

        # assumed behavior is union
        result = list(set(query_res[0]).union(*query_res[1:]))

        # for each document, get the score
        scored_results = {}
        for hit in result:
            scored_results[hit] = self.rank(query_string, hit)

        return scored_results

    def unit_search(self, query_unit, index):
        """ Searches a given index, returning the result of a single query 
        unit, be it a phrase query or a single word.
        
        Parameters:
        -----------
            query_unit : str
                Search unit, can be multiple words (phrase search) or single 
                words.
            index : 
                Reference to the index to be used, defines the category in 
                which the search is performed.

        Results:
        --------
            search_res : dict
                Dictionary containing the results and the positions of each 
                hit for the defined query.
        """
        # Create the result structure
        query_res = {}

        # if empty query
        if len(query_unit.split()) == 0:
            return {}

        # Check if single word instead of phrase query
        if len(query_unit.split()) == 1:
            query_res = index[query_unit] 
    
        else:       # phrase query  
            # obtain all the documents containing all the words
            doc_list = []
            for word in query_unit.split():
                doc_list.append([document_id for document_id in index[word].keys()])  
            doc_list = set(doc_list[0]).intersection(*doc_list[1:])

            # For each document, get the hits and their positions, then fetch
            # only those with the words in sequential order
            for hit in doc_list:
                # auxiliary structure to check if the words are sequentially arranged
                sequential_aux = []     
                for i, word in enumerate(query_unit.split()):
                    sequential_aux.append(np.subtract(index[word][hit], i))
                
                query_res[hit] = set(sequential_aux[0]).intersection(*sequential_aux[1:])
            
        # return only the non-empty
        return {k:v for k,v in query_res.items() if v}

    def rank(self, query_string, doc_id):
        """ Gigven a query string and a document id, returns a similarity 
        score that the query obtains using a certain metric (tf-idf) by default.

        Parameters:
        -----------
            query_string : str
                the query string on which to compare
            doc_id : str
                unique identifier for the document

        Returns:
            score : float
                a value corresponding to a similarity score between the query
                and the desired document.
        """
        score = 0

        # parse category queries
        # for author
        for match in self.author_pattern.findall(query_string):
            tmp = self.sanitize(match, cfg.AUTHOR_STEM, 
                                cfg.AUTHOR_STOP, cfg.AUTHOR_CASEFOLD)
            for word in tmp:
                score += self.tf_idf(word, doc_id, self.author_inv_index)
            
        # for title
        for match in self.title_pattern.findall(query_string):
            tmp = self.sanitize(match, cfg.TITLE_STEM, 
                                cfg.TITLE_STOP, cfg.TITLE_CASEFOLD)
            for word in tmp:
                score += self.tf_idf(word, doc_id, self.title_inv_index)

        # for publication
        for match in self.publication_pattern.findall(query_string):
            tmp = self.sanitize(match, cfg.PUBLICATION_STEM, 
                                cfg.PUBLICATION_STOP, cfg.PUBLICATION_CASEFOLD)
            for word in tmp:
                score += self.tf_idf(word, doc_id, self.publication_inv_index)

        # for content
        for match in self.content_pattern.findall(query_string):
            tmp = self.sanitize(match, cfg.CONTENT_STEM, 
                                cfg.CONTENT_STOP, cfg.CONTENT_CASEFOLD)
            for word in tmp:
                score += self.tf_idf(word, doc_id, self.content_inv_index)

        # if no matches were had yet, search for content
        if score == 0:
            tmp = self.sanitize(query_string, cfg.CONTENT_STEM, 
                                cfg.CONTENT_STOP, cfg.CONTENT_CASEFOLD)
            for word in tmp:
                score += self.tf_idf(word, doc_id, self.content_inv_index)   
                
        return score

    def tf_idf(self, word, document_id, index):
        """ Term frequency - Inverse document frequency
        Evaluates how important a word is for a document in a corpus.

        Parameters:
        -----------
            word : string
                word to use for ranking
            document_id : string
                document id to be used in ranking
        Returns:    
        --------
            tf_idf score : float
                tf-idf value 
        """
        # if no unit is present in document
        try:
            # get the frequency of a word in the document
            tf = len(self.unit_search(word, index)[document_id])

            # counts in how many documents of the corpus the word appears
            idf = math.log(self.n_documents / len(self.unit_search(word, index)))

            # calculate the tf-idf metric
            return tf * idf
        except KeyError:        # if the search term doesn't exist in the index
            return 0

    def save(self, filepath):
        """ Saves this SearchStructure object into a file for later use.

        Parameters:
        -----------
            filepath : str
                directory and filename on which to save this structure
        """
        with open(filepath, 'wb') as self_structure:
            pickle.dump(self, self_structure)

    @classmethod
    def load(cls, filepath):
        """ Loads a saved SearchStructure object from a specified path.

        Parameters:
        -----------
            filepath : str
                directory and filename on which the file to load the structure
                is located.

        Returns:
        --------
            struct : SearchStructure
                the desired search structure
        """
        with open(filepath, 'rb') as new_structure:
            struct = pickle.load(new_structure)
        
        return struct
