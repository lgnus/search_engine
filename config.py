""" This file controls options for the search engine """

__author__ = "Jose Carneiro"

from enum import Enum

# Regex options
QUERY_TOKENIZER = r'(".*?"|\w+)'
WORD_TOKENIZER = r'(\w+)'

# General options
N_RESULTS = 20

# NOTE: if stemming is turned on, casefold will be true regardless of the option
# Author parsing options
AUTHOR_STEM = False
AUTHOR_STOP = False
AUTHOR_CASEFOLD = True

# Content parsing options
CONTENT_STEM = True
CONTENT_STOP = True
CONTENT_CASEFOLD = True

# Title parsing options
TITLE_STEM = False
TITLE_STOP = False
TITLE_CASEFOLD = True

# Publication parsing options
PUBLICATION_STEM = False
PUBLICATION_STOP = False
PUBLICATION_CASEFOLD = True

class SearchType(Enum):
    AUTHOR = 0,
    TITLE = 1, 
    PUBLICATION = 2, 
    CONTENT = 3
