__author__ = "Jose Carneiro"

import os
import math
from collections import defaultdict

import pytest
from SearchStructure import SearchStructure

@pytest.fixture
def ii_empty():
    ii = SearchStructure()
    return ii


@pytest.fixture
def inv_index():
    ii = SearchStructure()

    keeper_db = [
        'The old night keeper keeps the keep in the town',
        'In the big old house in the big old gown.',
        'The house in the town had the big old keep',
        'Where the old night keeper never did sleep.',
        'The night keeper keeps the keep in the night',
        'And keeps in the dark and sleeps in the light.'
    ]

    ii.add_document('1', 'Anthony Jeselnik', 'Thoughts and Prayers', 'Folk', keeper_db[0])
    ii.add_document('2', 'Louis Blob', 'People Shop', 'Elitists United', keeper_db[1])
    ii.add_document('3', 'Jimmy Martell', 'Making People Laugh', 'Banana Limited', keeper_db[2])
    ii.add_document('4', 'Dara Brown', 'Crowd Tickler', 'Adventure Brocolli', keeper_db[3])
    ii.add_document('5', 'Russell Martell', 'Gone with the Wind', 'Peanuts Limited', keeper_db[4])
    ii.add_document('6', 'Ellen Brown', 'Joke People', 'Deviant United', keeper_db[5])

    return ii

@pytest.mark.add_doc
def test_add_document(ii_empty):
    """ Test the structure after document additions """

    # assert they all start empty
    assert ii_empty.content_inv_index == defaultdict()
    assert ii_empty.author_inv_index == defaultdict()
    assert ii_empty.title_inv_index == defaultdict()
    assert ii_empty.publication_inv_index == defaultdict()

    # add test document
    ii_empty.add_document('1', 'antonio', 'brocolli are the best', 'new york times', 'brocolli proven to be the best')

    # make sure results are as expected
    assert ii_empty.content_inv_index == defaultdict(dict, {'brocolli': {'1': [0]}, 'proven': {'1': [1]}, 'best': {'1': [2]}})
    assert ii_empty.author_inv_index == defaultdict(dict, {'antonio': {'1': [0]}})
    assert ii_empty.publication_inv_index == defaultdict(dict, {'new': {'1': [0]}, 'york': {'1': [1]}, 'times': {'1': [2]}})
    assert ii_empty.title_inv_index == defaultdict(dict, {'brocolli': {'1': [0]}, 'are': {'1': [1]}, 'the': {'1': [2]}, 'best': {'1': [3]}})

    # assert correct number of documents
    assert ii_empty.n_documents == 1

    # add another test document
    ii_empty.add_document('2', 'antonio', 'brocolli are the best', 'new york times', 'brocolli proven to be the best')

    # assert still correct
    assert ii_empty.content_inv_index == defaultdict(dict,{'brocolli': {'1': [0], '2': [0]}, 'proven': {'1': [1], '2': [1]}, 'best': {'1': [2], '2': [2]}})
    assert ii_empty.author_inv_index == defaultdict(dict, {'antonio': {'1': [0], '2': [0]}})
    assert ii_empty.title_inv_index == defaultdict(dict,{'brocolli': {'1': [0], '2': [0]}, 'are': {'1': [1], '2': [1]}, 'the': {'1': [2], '2': [2]}, 'best': {'1': [3], '2': [3]}})
    assert ii_empty.publication_inv_index == defaultdict(dict,{'new': {'1': [0], '2': [0]},'york': {'1': [1], '2': [1]},'times': {'1': [2], '2': [2]}})
   
    # assert correct number of documents
    assert ii_empty.n_documents == 2


@pytest.mark.sanitize
def test_sanitize(ii_empty):
    """
    Test all the different types of sanitization
    """
    assert ii_empty.sanitize('The Night "is dark and full" of Terrors', is_query=True) == ['The', 'Night', 'is dark and full', 'of', 'Terrors']
    assert ii_empty.sanitize('The Night is dark and full of Terrors', is_query=True) == ['The', 'Night', 'is', 'dark', 'and', 'full', 'of', 'Terrors']
    assert ii_empty.sanitize('The Night "is dark and full" of Terrors', casefolding=True, is_query=True) == ['the', 'night', 'is dark and full', 'of', 'terrors']
    assert ii_empty.sanitize('The Night "is dark and full" of Terrors', stem=True, is_query=True) == ['the', 'night', 'is dark and full', 'of', 'terror']
    assert ii_empty.sanitize('The Night "is dark and full" of Terrors', stopword_filter=True, is_query=True) == ['The', 'Night', 'dark full', 'Terrors']
    assert ii_empty.sanitize('The Night "is dark and full" of Terrors', stem=True, stopword_filter=True, is_query=True) == ['night', 'dark full', 'terror']
    assert ii_empty.sanitize('"The Night" is dark and "full of Terrors"', stem=True, stopword_filter=True, is_query=True) == ['night', 'dark', 'full terror']


@pytest.mark.search
def test_single_word_query(inv_index):
    """ Test single word queries on all the possible categories, ranking order is not tested
    here so only the correct hits are checked.
    """
    
    # test single word queries on content
    assert inv_index.search('') == (0, [])
    assert inv_index.search('brocolli') == (0, [])

    # content search, implicit and explicit
    matches, res = inv_index.search('big')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['2', '3'])

    matches, res = inv_index.search('light')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['6']) 

    # author search
    matches, res = inv_index.search('author:martell')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['5', '3'])

    # title search
    matches, res = inv_index.search('title:People')
    assert matches == 3
    assert set([hit[0] for hit in res]) == set(['6', '3', '2'])

    # publication search
    matches, res = inv_index.search('publication:brocolli')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['4'])


@pytest.mark.search
def test_multi_word_query(inv_index):
    """ Tests multi word queries, default behaviour is OR, since the ranking indirectly
    addresses the issue so as to put the better matches higher. 
    """
    # TODO make examples for when things shouldn't work out
    assert inv_index.search('absolute unit') == (0, [])

    # make basic single word queries on all categories
    # content search, implicit and explicit
    matches, res = inv_index.search('big old house')
    assert matches == 4
    assert set([hit[0] for hit in res]) == set(['1', '2', '3', '4'])

    matches, res = inv_index.search('content:night keeper')
    assert matches == 3
    assert set([hit[0] for hit in res]) == set(['1', '5', '4'])

    # author search
    matches, res = inv_index.search('author:anthony Brown')
    assert matches == 3
    assert set([hit[0] for hit in res]) == set(['1', '6', '4'])

    # publication search
    matches, res = inv_index.search('publication:peanuts folk')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['1', '5'])

    # title search
    matches, res = inv_index.search('title:prayers joke')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['1', '6'])


@pytest.mark.search
def test_phrase_search_query(inv_index):
    """ Test phrase search queries.
    """
    matches, res = inv_index.search('"big old house"')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['2'])

    matches, res = inv_index.search('"big old house" dark')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['2', '6'])

    matches, res = inv_index.search('author:"louis"')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['2'])

    matches, res = inv_index.search('author:"Brown" martell')
    assert matches == 4
    assert set([hit[0] for hit in res]) == set(['3', '4', '5', '6'])

    matches, res = inv_index.search('author:"Brown martell"')
    assert matches == 0
    assert set([hit[0] for hit in res]) == set([])


@pytest.mark.search
def test_multi_category_query(inv_index):
    """ Test queries using multiple categories
    """
    matches, res = inv_index.search('author:"anthony" title:prayers')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['1'])

    matches, res = inv_index.search('title:prayers author:jeselnik content:"dark and sleeps"')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['1', '6'])

    matches, res = inv_index.search('title:gone with the wind title:joke people')
    assert matches == 4
    assert set([hit[0] for hit in res]) == set(['2', '3', '5', '6'])

    matches, res = inv_index.search('author:"Jimmy Martell" content: dark')
    assert matches == 2
    assert set([hit[0] for hit in res]) == set(['3', '6'])

    matches, res = inv_index.search('author:"Jimmy Martell" brown content:"night keeper"')
    assert matches == 5
    assert set([hit[0] for hit in res]) == set(['1', '3', '4', '5', '6'])


@pytest.mark.search
def test_boolean_search(inv_index):

    matches, res = inv_index.search('dark OR keeper')
    assert matches == 4
    assert set([hit[0] for hit in res]) == set(['1', '4', '5', '6'])

    matches, res = inv_index.search('content:dark OR content:keeper')
    assert matches == 4
    assert set([hit[0] for hit in res]) == set(['1', '4', '5', '6'])

    matches, res = inv_index.search('keeper AND night')
    assert matches == 3
    assert set([hit[0] for hit in res]) == set(['1', '4', '5'])

    matches, res = inv_index.search('author:brown NOT publication:deviant')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['4'])

    matches, res = inv_index.search('title:tickler AND brocolli')
    assert matches == 0
    assert set([hit[0] for hit in res]) == set([])   

    matches, res = inv_index.search('title:tickler AND ')
    assert matches == 0
    assert set([hit[0] for hit in res]) == set([])    

    matches, res = inv_index.search('title:tickler OR ')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['4'])  

    matches, res = inv_index.search('title:tickler OR author:martell AND publication:banana')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['3'])  

    matches, res = inv_index.search('publication:Limited AND author:martell NOT author:Russell')
    assert matches == 1
    assert set([hit[0] for hit in res]) == set(['3'])   

@pytest.mark.rank
def test_tf_idf(inv_index):

    assert inv_index.tf_idf('martell', '0', inv_index.author_inv_index) == 0
    assert inv_index.tf_idf('martell', '3', inv_index.author_inv_index) == 1 * math.log(6 / 2)
    assert inv_index.tf_idf('', '0', inv_index.author_inv_index) == 0


@pytest.mark.save_load
def test_save_and_load(inv_index):
    # save index
    inv_index.save('test')

    # load index
    new_index = SearchStructure.load('test')

    # make sure the results are the same
    assert new_index.n_documents == inv_index.n_documents
    assert new_index.content_inv_index == inv_index.content_inv_index
    assert new_index.author_inv_index == inv_index.author_inv_index
    assert new_index.publication_inv_index == inv_index.publication_inv_index
    assert new_index.title_inv_index == inv_index.title_inv_index

    # remove the created file
    os.remove('test')

